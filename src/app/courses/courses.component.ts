import { CoursesService } from './../service/courses.service';
import { Component, OnInit } from '@angular/core';
// Core libraries

// Decorator function
@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})

// Export Class function
export class CoursesComponent implements OnInit {
  arrayCourses= [
    { id:1, name: 'course-1' },
    { id:2, name: 'course-2' },
    { id:3, name: 'course-3' },
  ];
  isActive = true;
  title = "List of Courses";
  courses;
  emailStatic = "me@email.com"
  textContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."

  onKeyUpConsole() {
    console.log(this.emailStatic);
  }

  onKeyUpEmail(email) {
    console.log(email);
  }

  onKeyUp() {
    // if($event.keyCode === 13) console.log('Enter was pressed');
    console.log('Enter was pressed');
  }

  onDiveClicked() {
    console.log('div was clicked!!')
  }
  onSave($event) {
    $event.stopPropagation();
    console.log('Button was clicked', $event);
  }
  constructor(service: CoursesService) {
    // let service = new CoursesService();
    this.courses = service.getCourses();
  }
  onAdd() {
    this.arrayCourses.push({id: 4, name: 'Course4'})
  }
  onRemove(testing) {
    let index = this.arrayCourses.indexOf(testing)
    this.arrayCourses.splice(index);
  }
  ngOnInit() {
  }

}
