import { AppErrorHandler } from './common/app-error-handler';
import { SummaryPipe } from './summary.pip';
import { CoursesService } from './service/courses.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { CoursesComponent } from './courses/courses.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FavoriteComponent } from './favorite/favorite.component';
import { PanelComponent } from './panel/panel.component';
import { InputFormatDirective } from './input-format.directive';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { NewCourseFormComponent } from './new-course-form/new-course-form.component';
import { PostsComponent } from './posts/posts.component';
import { PostService } from './service/post.service';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { GithubprofileComponent } from './githubprofile/githubprofile.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { GithubfollowersComponent } from './githubfollowers/githubfollowers.component';
import { GithubusersService } from './service/githubusers.service';

@NgModule({
   declarations: [
      AppComponent,
      CoursesComponent,
      SummaryPipe,
      FavoriteComponent,
      PanelComponent,
      InputFormatDirective,
      ContactFormComponent,
      SignUpComponent,
      NewCourseFormComponent,
      PostsComponent,
      NavbarComponent,
      HomeComponent,
      GithubprofileComponent,
      NotfoundComponent,
      GithubfollowersComponent
   ],
   imports: [
      BrowserModule,
      FormsModule,
      AngularFontAwesomeModule,
      ReactiveFormsModule,
      HttpModule,
      RouterModule.forRoot([
         { path: '', component: HomeComponent },
         { path: 'followers/:id/:username', component: GithubprofileComponent },
         { path: 'followers', component: GithubfollowersComponent },
         { path: 'posts', component: PostsComponent },
         { path: '**', component: NotfoundComponent },
      ])
   ],
   providers: [
      PostService,
      CoursesService,
      GithubusersService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
