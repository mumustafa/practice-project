import { FavoriteChangedEventArgs } from './favorite/favorite.component';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  viewMode = 'otherwise';
  title = 'Angular app';
  post = {
    title: "title",
    isFavorite: true
  }
  courses = [1,2]
  onFavoriteChanged(eventArgs: FavoriteChangedEventArgs){
    console.log("Favorite Change", eventArgs)
  }
}
