import { BadInput } from './../common/bad-input';
import { AppError } from './../common/app-error';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { NotFoundError } from '../common/not-found-error';

@Injectable({
  providedIn: 'root'
})

export class DataService {
  constructor(private url: string, private http: Http) { }

  // Getting Post
  getAll() {
    return this.http.get(this.url)
      .map(response => response.json())
      .catch(this.handleError)
  }

  // Create Post
  create(resource) {
    return this.http.post(this.url, JSON.stringify(resource))
    .map(response => response.json())  
    .catch(this.handleError);
  }

  // Update Post
  update(resource) {
    return this.http.patch(this.url + '/' + resource.id, JSON.stringify({ isRead: true }))
    .map(response => response.json())  
    .catch(this.handleError);
  }

  // Delete Post
  delete(id) {
    return this.http.delete(this.url + '/' + id)
    .map(response => response.json())  
    .catch(this.handleError);
  }

  private handleError(error: Response) {
    if (error.status === 400)
      return Observable.throw(new BadInput(error.json()))

    if (error.status === 404)
      return Observable.throw(new NotFoundError())

    return Observable.throw(new AppError(error));
  }
}