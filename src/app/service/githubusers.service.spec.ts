/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GithubusersService } from './githubusers.service';

describe('Service: Githubusers', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GithubusersService]
    });
  });

  it('should ...', inject([GithubusersService], (service: GithubusersService) => {
    expect(service).toBeTruthy();
  }));
});
