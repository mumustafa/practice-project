import { Observable } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { GithubusersService } from '../service/githubusers.service';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-githubfollowers',
  templateUrl: './githubfollowers.component.html',
  styleUrls: ['./githubfollowers.component.css']
})
export class GithubfollowersComponent implements OnInit {
  users: any[];
  constructor(
    private route: ActivatedRoute,
    private service: GithubusersService
  ) { }

  ngOnInit() {
    Observable.combineLatest([
      this.route.paramMap,
      this.route.queryParamMap
    ])
      .switchMap(combined => {
        let id = combined[0].get('id');
        let page = combined[1].get('page');

        return this.service.getAll();
      })
      .subscribe(users => this.users = users);

    // this.route.paramMap
    //   .subscribe(params => {

    //   });

    // this.route.queryParamMap
    //   .subscribe(params => {

    //   })

  }

}
